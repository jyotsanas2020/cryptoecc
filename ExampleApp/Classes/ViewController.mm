//
//  ViewController.m
//  cryptoppdemo
//
//Created by Sandeep Aggarwal on 14/06/15.
//

#import "ViewController.h"
#import "CryptoppECC.h"
#import "CryptoppECDSA.h"


//new one

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    //CryptoppECC* ecc=[[CryptoppECC alloc] init];
    //[ecc randomKeysEncryptDecrypt];
//
//    NSString* value = @"MGUCMBu3GWA9AZGMp0YtUv/GZfzkAJ7ch3qMw+v1uiixDkGKNoO3JuL2BIaD8SW0C0x/ZwIxAIS4OAS+4QfOJk7XCqb+jFT4Yq+p9PJGMNacCB0zvZ59sVfU7F5xsQ2r+Om+tPbSTg==";

    NSString *PRIVATE_KEY_1 = @"49b61e28a3569638f9743267f846dee568a3743ba8aa10298ab5f1d3ad325bae";
    NSString* path = [[NSBundle mainBundle] pathForResource:@"test"
                                                     ofType:@"txt"];
    NSString* content = [NSString stringWithContentsOfFile:path
                                                  encoding:NSUTF8StringEncoding
                                                     error:NULL];
   
    

//    NSDictionary *JSONDic = [NSDictionary
//                dictionaryWithObjectsAndKeys:value,@"license",
//                                            nil];
//
//    NSError *error;
//    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:JSONDic
//                                                       options:NSJSONWritingPrettyPrinted
//                                                         error:&error];
//    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
     CryptoppECC* ecc = [[CryptoppECC alloc] init];
     NSString* decryptedString = [ecc decrypt:content :PRIVATE_KEY_1 curve:CurveType_secp256r1];
}

//- (void)didReceiveMemoryWarning {
//    [super didReceiveMemoryWarning];
//    // Dispose of any resources that can be recreated.
//}

@end
